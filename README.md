# Travail Pratique n°2 - SIO #
##Description##
L'exécutable

*DerangementMonteCarlo.jar*

contient le programme implémenté pour le travail pratique n°2 de SIO.

##Configuration##
Il est possible de configurer le programme en lui fournissant à son exécution en console le chemin vers un fichier de configuration. Un fichier d'exemple (exemple.conf) se trouve dans le même répertoire. 

**Par exemple**: 
```
#!shell

java -jar DerangementMonteCarlo.jar exemple.conf

```

Si ce chemin n'est pas spécifié, la configuration par défaut (correspondant aux consignes) sera utilisée.

##Choix de la partie##
Au moment de l'exécution, il est possible de choisir quelle partie du travail pratique on désire exécuter en tapant 1 ou 2.