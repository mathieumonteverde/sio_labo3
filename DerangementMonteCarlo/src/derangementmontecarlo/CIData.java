package derangementmontecarlo;

/**
 * Cette classe permet de représenter des intervalles de confiance en spécifiant
 * la valeur centrale de l'intervalle et sa largeur.
 * 
 * @author Mathieu Monteverde
 */
public class CIData {
   // Milieu de l'intervalle
   private final double mean;
   // Largeur
   private final double width;
   
   /**
    * Construire l'intervalle de confiance.
    * @param mean le milieur de l'intervalle
    * @param width largeur de l'intervalle
    */
   public CIData(double mean, double width) {
      this.mean = mean;
      this.width = width;
   }
   
   /**
    * Récupérer la moyenne de l'intervalle
    * @return la moyenne
    */
   public double getMean() {
      return mean;
   }
   
   /**
    * Récupérer la largeur de l'intervalle
    * @return la largeur
    */
   public double getWidth() {
      return width;
   }
}
