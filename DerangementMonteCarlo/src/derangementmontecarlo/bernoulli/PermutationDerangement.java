package derangementmontecarlo.bernoulli;

import java.util.Random;

/**
 * Cette classe représente une épreuve de Bernoulli particulière qui consiste à 
 * effectuer une permutation d'un senemble de nombres dans un tableau, et de 
 * regarder si le résultat est un dérangement. L'épreuve est considérée comme
 * un succès le cas échéant.
 * 
 * @author Mathieu Monteverde
 */
public class PermutationDerangement extends Bernoulli {

   // Ensemble de valeurs
   private int[] set;
   // Générateur aléatoire
   private Random rand;
   
   /**
    * Csontructeur. Création de l'ensemble de nombres de 0 à setSize. Initialisation
    * d'un générateur aléatoire utilisant seed comme graine.
    * @param setSize la taille de l'ensemble
    * @param seed graine du générateur aléatoire
    */
   public PermutationDerangement(int setSize, long seed) {
      // Vérifier la taille
      if (setSize <= 0) {
         throw new RuntimeException("Le nombre d'éléments ne peut pas être négative ou nulle.");
      }
      // Créer l'ensemble d'éléments
      set = new int[setSize];
      // Initialisaer l'ensemble
      for (int i = 0; i < setSize; ++i) {
         set[i] = i;
      }
      // Initialiser le générateur aléatoire
      rand = new Random(seed);
   }
   
   /**
    * Effectuer une épreuve de bernoulli
    * @return 
    */
   @Override
   public boolean bernoulliTrial() {
      return shuffle(set, rand);
   }
   
   /**
    * Mélange le tableau passé en paramètre en utilisant le générateur aléatoire
    * passé en apramètre.
    *
    * @param array le tableau à mélanger
    * @param rand le générateur aléatoire à utiliser
    */
   private static boolean shuffle(int[] array, Random rand) {
      for (int i = 0; i < array.length; ++i) {
         // Choisir un index aléatoire emtre i et la taille du tableau
         int index = rand.nextInt(array.length - i) + i;

         // Permuter avec le ie élément
         int temp = array[i];
         array[i] = array[index];
         array[index] = temp;
         
         if (array[i] == i) {
            return false;
         }
      }
      
      return true;
   }

}
