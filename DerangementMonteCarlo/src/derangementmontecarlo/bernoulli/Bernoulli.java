package derangementmontecarlo.bernoulli;

/**
 * Cette classe abstraite représente une généralisation d'épreuve de 
 * Bernoulli. Un objet issu de cette hiéerarchie propose d'effectuer une ou 
 * plusieurs épreuves de Bernoullis. 
 * 
 * La classe abstraite peut garder un compte des épreuves effectués 
 * et du nombre de succès rencontrés au cours de ces épreuves.
 * 
 * @author Mathieu Monteverde
 */
public abstract class Bernoulli {
   
   // Nombre de succès rencontrées depuis la création de l'objet
   private long success;
   // Nombre d'épreuves depuis la création de l'objet
   private long trials;
   
   /**
    * Constructeur. Initialise le nombre de succès et d'épruves à 0
    */
   public Bernoulli() {
      success = 0;
      trials = 0;
   }

   /**
    * Méthode abstraite devant effectuer une épreuve de Bernoulli. Le résultat 
    * n'est pas stocké dans le nombre de succès, ni dans le nombre d'épreuves. 
    * 
    * Utiliser cette méthode lorsque l'on veut effectuer un essai de l'expérience,
    * sans qu'elle soit prise en compte dans les propriétés de l'objet.
    * @return true si l'épreuve était un succès, false sinon
    */
   public abstract boolean bernoulliTrial();
   
   /**
    * Effectuer plusieurs épreuves de Bernoulli. Les résultats sont pris en compte 
    * dans le total de succès et d'épreuves de l'objet.
    * 
    * Utiliser cette méthode lorsqu'on désire que l'objet garde trace 
    * des épreuves et succès réalisés.
    * @param nbOfTrials le nombre d'épreuves supplémentaire à réaliser
    * @return le nombre de succès rencontrés depuis la création de l'objet.
    */
   public long bernouliTrials(long nbOfTrials) {
      // Faire toutes les permutations aléatoires autant de fois que demandé
      for (long i = 0; i < nbOfTrials; ++i) {
         ++trials;
         if (bernoulliTrial()) {
            ++success;
         }
      }
      
      // Retourner le nombre de succès
      return success;
   }

   /**
    * Retourne le nombre d'épreuves réalisées depuis la création de l'objet
    * @return le nombre d'épreuves
    */
   public long getTrials() {
      return trials;
   }
   
   /**
    * Retourne le nombre de succès rencontrés depuis la création de l'objet.
    * @return le nombre de succès
    */
   public long getSuccess() {
      return success;
   }
}
