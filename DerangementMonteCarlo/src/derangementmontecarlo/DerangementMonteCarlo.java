package derangementmontecarlo;

import derangementmontecarlo.bernoulli.PermutationDerangement;
import derangementmontecarlo.settings.Settings;
import java.util.Scanner;

/**
 *
 * @author Mathieu Monteverde
 */
public class DerangementMonteCarlo {

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args) {

      Settings settings;

      /*
         Afficher les instructions si l'utilisateur n'a pas fourni de paramètre
         ou s'il en a fourni trop.
       */
      if (args.length == 1) {
         System.out.println("Lecture du fichier de configuration fourni...");
         settings = new Settings(args[0]);
      } else {
         System.out.println("Lecture de la configuration par défaut...");
         settings = new Settings();
      }

      /*
         Lire la commande utilisateur.
       */
      Scanner reader = new Scanner(System.in);
      System.out.println("----------------------------------------------------");
      System.out.println("Liste des commandes:");
      System.out.println("----------------------------------------------------");
      System.out.println("1 - Expérience partie 1");
      System.out.println("2 - Expérience partie 2");
      System.out.println("----------------------------------------------------");

      int cmd = reader.nextInt();

      switch (cmd) {
         case 1:
            MonteCarloSimulation m = new MonteCarloSimulation(new PermutationDerangement(settings.getM(), settings.getSeed()), settings);
            CIData ConfidenceInterval = m.simulateConfidenceIterval();
            break;

         case 2:
            break;
         
         default:
            System.out.println("Commande inconnue...");
            break;
      }

   }

}
