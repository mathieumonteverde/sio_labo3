package derangementmontecarlo;

import derangementmontecarlo.bernoulli.Bernoulli;
import derangementmontecarlo.settings.Settings;
import format.OutputFormatter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigDecimal;
import static java.math.RoundingMode.FLOOR;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.ProgressBar;

/**
 * Cette classe représente une simulation de Monte Carlo et plusieurs
 * fonctionnalités qui lui sont liées.
 *
 * @author Mathieu Monteverde
 */
public class MonteCarloSimulation {

   // L'épreuve de Bernoulli à effectuer
   private Bernoulli bernoulli;
   // les propriétés à utiliser
   private Settings settings;

   // Valeur du quantile normal à delta/2 = 2.5%
   private static final double Z = 1.95996;

   // Valeur du quantile normal à delta/2
   private static final double QUANTILE_HALF_DELTA = 2.5;

   private static final String CIData = OutputFormatter.getFormatFromResource("/result_format.txt");

   private static final String step1 = OutputFormatter.getFormatFromResource("/partie1_format.txt");
   private static final String CIEvolution = OutputFormatter.getFormatFromResource("/evolution_format.txt");

   private static DecimalFormat df = new DecimalFormat("######.######");

   /**
    * Constructeur.
    *
    * @param bernoulli l'expérience de Bernoulli à utiliser
    * @param settings les propriétés à utiliser
    */
   public MonteCarloSimulation(Bernoulli bernoulli, Settings settings) {
      this.bernoulli = bernoulli;
      this.settings = settings;
      df.setRoundingMode(FLOOR);
   }

   /**
    * Méthode effectuant un premier échantillon de l'expérience de Bernoulli
    * définie.
    *
    * Elle estime ensuite la taille de l'échantillon minimal à calculer pour
    * pouvoir établir un estimateur de la variable aléatoire liée à l'épreuve de
    * Bernoulli nommée ci-dessus avec un intervalle de confiance de largeur
    * maximum valant la propriété deltaMax de l'objet Settings défini dans le
    * constructeur.
    *
    * La méthode effectue esnuite les échantillons de manière incrémentale
    * jusqu'à atteindre cet échantillon estimé et retourne un tableau de
    * résultats de type CIData
    *
    * @return un tableau de résultat d'objets CIData
    */
   public CIData simulateConfidenceIterval() {
      Writer results = null;
      Writer evolution = null;
      Writer console = null;

      try {

         // Créer le dossier de sortie
         new File(settings.getPart1ResultPath()).mkdirs();

         // Créer les Writer de sortie
         results = createWriter("./partie1/results.txt");
         evolution = createWriter("./partie1/evolution.csv");
         evolution.write("Taille échantillon,Estimation p,Borne inf.,Borne sup.,Largeur IC\n");

         // Ouvrir un Writer vers la console
         console = new BufferedWriter(new OutputStreamWriter(System.out, "UTF-8"));

         /*
            --------------------------------------------------------------------
            Etape 1
            --------------------------------------------------------------------
          */
         displayStep(results, "Etape 1");
         displayStep(console, "Etape 1");
         // Créer un premieréchantillon de mesures
         long success = bernoulli.bernouliTrials(settings.getnInit());
         double mean = (double) success / bernoulli.getTrials();

         // Calculer l'intervalle de confiance correspondant et logger le résultat
         CIData ci = getCIData(mean, bernoulli.getTrials());
         write(console, formatCIData(CIData, bernoulli.getTrials(), mean, ci));
         write(evolution, formatCIData(CIEvolution, bernoulli.getTrials(), mean, ci));
         write(results, formatCIData(CIData, bernoulli.getTrials(), mean, ci));

         /*
            --------------------------------------------------------------------
            Etape 2
            --------------------------------------------------------------------
          */
         // Estimer la taille de l'échantillon à mesurer
         displayStep(results, "Etape 2");
         displayStep(console, "Etape 2");
         long N = estimateSampleSize(mean, settings.getDeltaMax(), settings.getnSupp());
         write(results, "Estimation de la taille N de l'échantillon: " + N);
         write(console, "Estimation de la taille N de l'échantillon: " + N);

         /*
            --------------------------------------------------------------------
            Etape 3
            --------------------------------------------------------------------
          */
         displayStep(results, "Etape 3");
         write(results, "Création de l'échantillon...\n");
         displayStep(console, "Etape 3");
         write(console, "Création de l'échantillon...\n");

         /*
            Effectuer les N - 1000 échantillons supplémentaires nécessaires
            par en effectuant settings.getNbSamples() échantillons au long
            de la progession.
          */
         // Caluler la taille du pas
         long step = (N - 1000) / settings.getNbSamples();
         step += (N - 1000) % settings.getNbSamples() == 0 ? 1 : 0;
         while (bernoulli.getTrials() < N) {
            // Afficher la barre de progression
            progressBar((double) bernoulli.getTrials() / N);

            // Calculer le nombre d'échantillons à faire et effectuer les épreuves de Bernoulli
            long nbSamples = step < N - bernoulli.getTrials() ? step : N - bernoulli.getTrials();
            success = bernoulli.bernouliTrials(nbSamples);

            // Calculer l'intervalle de confiance atteint
            ci = getCIData((double) success / bernoulli.getTrials(), bernoulli.getTrials());
            // Logger le resultat dans l'évolution du IC
            write(evolution, formatCIData(CIEvolution, bernoulli.getTrials(), (double) success / bernoulli.getTrials(), ci));

         }
         System.out.println("");

         // Tant qu'on a pas atteint l'intervalle recherché, on continue
         while (ci.getWidth() > settings.getDeltaMax()) {
            // Effectuer les épreuves et calculer l'IC correspondant
            success = bernoulli.bernouliTrials(settings.getnSupp());
            ci = getCIData((double) success / bernoulli.getTrials(), bernoulli.getTrials());

            // Logger les résultats
            write(results, formatCIData(CIData, bernoulli.getTrials(), (double) success / bernoulli.getTrials(), ci));
            write(console, formatCIData(CIData, bernoulli.getTrials(), (double) success / bernoulli.getTrials(), ci));
            write(evolution, formatCIData(CIEvolution, bernoulli.getTrials(), (double) success / bernoulli.getTrials(), ci));
         }

         /*
         --------------------------------------------------------------------
         Etape 3
         --------------------------------------------------------------------
          */
         // Retourne le résultat
         displayStep(results, "Etape 4");
         displayStep(console, "Etape 4");
         write(results, formatCIData(CIData, bernoulli.getTrials(), (double) bernoulli.getSuccess() / bernoulli.getTrials(), ci));
         write(console, formatCIData(CIData, bernoulli.getTrials(), (double) bernoulli.getSuccess() / bernoulli.getTrials(), ci));

         results.close();
         evolution.close();
         console.close();
         
         return ci;
      } catch (FileNotFoundException ex) {
         Logger.getLogger(MonteCarloSimulation.class.getName()).log(Level.SEVERE, null, ex);
      } catch (UnsupportedEncodingException ex) {
         Logger.getLogger(MonteCarloSimulation.class.getName()).log(Level.SEVERE, null, ex);
      } catch (IOException ex) {
         Logger.getLogger(MonteCarloSimulation.class.getName()).log(Level.SEVERE, null, ex);
      }
      
      return null;
   }

   /**
    * Calcule l'intervalle de confiance correspondant à la moyenne et à la
    * taille de l'échantillon passé en paramètre.
    *
    * @param mean moyenne des mesures
    * @param N taille de l'échantillon
    * @return les données de l'intervalle de confiance dans un objet CIData
    */
   public static CIData getCIData(double mean, long N) {
      // Calculer la variance
      double deviation = Math.sqrt(mean * (1 - mean));
      // Calculer la largeur de l'intervalle de confiance
      double CIWidth = 2 * Z * (deviation / Math.sqrt(N));
      // Retourner les données
      return new CIData(mean, CIWidth);
   }

   /**
    * Estime la taille de l'échantillon nécessaire pour obtenir un intervalle de
    * confiance de largeur maximum deltaMax avec la moyenne donnée en paramètre.
    *
    * @param mean la moyenne des mesures
    * @param deltaMax la largeur maximal de l0intervalle de confiance
    * @return l'estimation de la taille de l'échantillon
    */
   private static long estimateSampleSize(double mean, double deltaMax, long roundUp) {

      double deviation = Math.sqrt(mean * (1 - mean));
      double sampleSize = Math.pow(2 * Z * deviation / deltaMax, 2);

      return (long) Math.ceil(sampleSize / roundUp) * roundUp;
   }

   /**
    * Créer un Writer vers un fichier de sortie.
    *
    * @param filePath
    * @return
    */
   private static Writer createWriter(String filePath) {

      try {
         // Ouvrir un fichier pour y écrire le résultat de la simulation
         File f = new File(filePath);
         f.createNewFile();
         return new BufferedWriter(new OutputStreamWriter(
                 new FileOutputStream(f), "UTF-8"));
      } catch (IOException ex) {
         Logger.getLogger(MonteCarloSimulation.class.getName()).log(Level.SEVERE, null, ex);
      }
      return null;
   }

   /**
    * Ecrire une String dans un Writer et flush le résultat
    *
    * @param out
    * @param s
    */
   private static void write(Writer out, String s) {
      try {
         out.write(s);
         out.flush();
      } catch (IOException ex) {
         Logger.getLogger(MonteCarloSimulation.class.getName()).log(Level.SEVERE, null, ex);
      }
   }

   /**
    * Formater les informations concernant un intervalle de confiance pour
    * l'affichage
    *
    * @param sampleSize
    * @param mean
    * @param ci
    * @return
    */
   private static String formatCIData(String format, long sampleSize, double mean, CIData ci) {
      return String.format(format,
              sampleSize,
              df.format(mean),
              df.format(ci.getMean() - ci.getWidth() / 2),
              df.format(ci.getMean() + ci.getWidth() / 2),
              df.format(ci.getWidth())
      );
   }

   /**
    * Afficher une bar de progression pendant l'échantillonnage
    *
    * @param percent
    */
   private static void progressBar(double percent) {

      final int width = 20;

      System.out.printf("\r%2.2f%s", percent * 100, "%");
   }

   /**
    * Afficher le nom de l'étape actuelle.
    *
    * @param name
    */
   private static void displayStep(Writer out, String name) {
      try {
         out.write("\n");
         out.write("----------------------------------------------------\n");
         out.write(name + "\n");
         out.write("----------------------------------------------------\n");
         out.flush();
      } catch (IOException ex) {
         Logger.getLogger(MonteCarloSimulation.class.getName()).log(Level.SEVERE, null, ex);
      }
   }
}
