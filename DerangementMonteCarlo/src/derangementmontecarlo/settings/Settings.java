package derangementmontecarlo.settings;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Cette classe représente les paramètres d'une simulation de Monte-Carlo.
 *
 * @author Mathieu Monteverde
 */
public class Settings {

   // Taille de l'ensemble
   private int M;
   // Graine du générateur aléatoire
   private long seed;
   // Taille initiale de l'échantillon des permutations
   private int nInit;
   // Demi largeur de l'intervalle de confiance maximum
   private double deltaMax;
   // Incrément supérieur si deltaMax paa atteint
   private int nSupp;
   // Taille d'échantillon deuxième partie
   private int N;
   // Nombre de de réplications
   private int K;
   // Chemin du fichier de résultat de la 1ère partie
   private String part1ResultPath;
   // Nombre d'échantillons de permutations à logger pendant la partie 1
   private int nbSamples;

   // Etats de lecture des propriétés
   private enum settingsStage {
      EMPTY,
      M,
      SEED,
      N_INIT,
      DELTA_MAX,
      N_SUPP,
      N,
      K,
      PART1_RESULT,
      NB_SAMPLES
   };

   /**
    * Créer l'instance des settings
    */
   public Settings() {
      BufferedReader in = null;
      try {
         InputStream input = getClass().getResourceAsStream("/default.conf");
         in = new BufferedReader(new InputStreamReader(input, "UTF8"));
         setConfiguration(in);
      } catch (UnsupportedEncodingException ex) {
         Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
      } finally {
         try {
            in.close();
         } catch (IOException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
         }
      }
   }

   public Settings(String confPath) {
      
      BufferedReader in = null;
      try {
         in = new BufferedReader(new InputStreamReader(new FileInputStream(confPath), "UTF8"));
         setConfiguration(in);
      } catch (UnsupportedEncodingException ex) {
         Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
      } catch (FileNotFoundException ex) {
         Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
      } finally {
         try {
            in.close();
         } catch (IOException ex) {
            Logger.getLogger(Settings.class.getName()).log(Level.SEVERE, null, ex);
         }
      }
   }

   /**
    * Définir la configuration des Settings.
    *
    * @param confPath le chemin vers un fichier de configuration
    */
   public final void setConfiguration(BufferedReader in) {
      try {

         String str;

         settingsStage current = settingsStage.M;

         while ((str = in.readLine()) != null) {

            if (str.isEmpty() || str.charAt(0) == '#' || str.charAt(0) == ' ') {
               continue;
            }

            switch (current) {
               case M:
                  this.M = Integer.parseInt(str);
                  current = settingsStage.SEED;
                  break;
               case SEED:
                  this.seed = Long.decode(str);
                  current = settingsStage.N_INIT;
                  break;
               case N_INIT:
                  this.nInit = Integer.parseInt(str);
                  current = settingsStage.DELTA_MAX;
                  break;
               case DELTA_MAX:
                  this.deltaMax = Double.parseDouble(str);
                  current = settingsStage.N_SUPP;
                  break;
               case N_SUPP:
                  this.nSupp = Integer.parseInt(str);
                  current = settingsStage.N;
                  break;
               case N:
                  this.N = Integer.parseInt(str);
                  current = settingsStage.K;
                  break;
               case K:
                  this.K = Integer.parseInt(str);
                  current = settingsStage.PART1_RESULT;
                  break;
               case PART1_RESULT:
                  this.part1ResultPath = str;
                  current = settingsStage.NB_SAMPLES;
                  break;
               case NB_SAMPLES:
                  nbSamples = Integer.parseInt(str);
                  current = settingsStage.EMPTY;
                  break;
            }
         }

         in.close();
      } catch (UnsupportedEncodingException e) {
         System.out.println(e.getMessage());
      } catch (IOException e) {
         System.out.println(e.getMessage());
      } catch (Exception e) {
         System.out.println(e.getMessage());
      }
   }

   /**
    * @return the M
    */
   public int getM() {
      return M;
   }

   /**
    * @return the seed
    */
   public long getSeed() {
      return seed;
   }

   /**
    * @return the nInit
    */
   public int getnInit() {
      return nInit;
   }

   /**
    * @return the deltaMax
    */
   public double getDeltaMax() {
      return deltaMax;
   }

   /**
    * @return the nSupp
    */
   public int getnSupp() {
      return nSupp;
   }

   /**
    * @return the N
    */
   public int getN() {
      return N;
   }

   /**
    * @return the K
    */
   public int getK() {
      return K;
   }
   
   public String getPart1ResultPath() {
      return part1ResultPath;
   }
   
   public int getNbSamples() {
      return nbSamples;
   }
}
