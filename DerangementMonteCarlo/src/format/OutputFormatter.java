/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package format;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mathieu
 */
public class OutputFormatter {

   public static String getFormatFromResource(String resourcePath) {
      String format = "";

      InputStream input = OutputFormatter.class.getResourceAsStream(resourcePath);
      try {
         BufferedReader in = new BufferedReader(new InputStreamReader(input, "UTF8"));
         String str;
         while ((str = in.readLine()) != null) {
            format += str + System.lineSeparator();
         }
      } catch (UnsupportedEncodingException ex) {
         Logger.getLogger(OutputFormatter.class.getName()).log(Level.SEVERE, null, ex);
      } catch (IOException ex) {
         Logger.getLogger(OutputFormatter.class.getName()).log(Level.SEVERE, null, ex);
      }

      return format;
   }
}
